import * as PIXI from 'pixi.js'
import Resources from './Resources'

export default class Game {
    constructor(loadedCallback) {
        this._app = new PIXI.Application(128*8+64, 128*4+64);

        for (const name in Resources) {
            PIXI.loader.add(name, Resources[name]);
        }

        console.log("Loading resources");
        PIXI.loader.load((loader, resources) => {
            this._resources = resources;
            loadedCallback(resources);
        });
    }

    setScene(scene) {
        scene.init(this._resources);
        this._app.stage.removeChildren();
        this._app.stage.addChild(scene);
    }


    getView() {
        return this._app.view;
    }

    getScreen() {
        return this._app.screen;
    }

    getTicker() {
        return this._app.ticker;
    }
}