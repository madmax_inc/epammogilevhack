

export default class Path {
    constructor() {
        this.t = 0.0;
    }

    advance(dt) {
        this.t += dt;

        return {
            pos: {
                x: (7 *(this.t / 1000)) * 128,
                y: 1.5 * 128
            },
            heading: Math.PI / 2
        };
    }

}