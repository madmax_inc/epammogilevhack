import * as PIXI from 'pixi.js'

export default class Scene extends PIXI.Container {
    constructor(game) {
        super();

        this._game = game;
    }

    init(resources) {
        //Use the resources here
    }

}