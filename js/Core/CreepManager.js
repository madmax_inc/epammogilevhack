import Path from "./Path";


export default class CreepManager {
    constructor(waveSize, spawnDuration, waveDuration) {
        this.t = 0.0;
        this.spawned = 0;

        this._waveSize = waveSize;
        this._spawnDuration = spawnDuration;
        this._waveDuration = waveDuration;

        this._mainPath = new Path();
    }

    setScene(scene) {
        this._scene = scene;
    }

    setMainPath(path) {
        this._mainPath = path;
    }

    update(dt) {
        this.t += dt;

        if (this.spawned < this._waveSize && this.t > this._spawnDuration) {
            this._scene.spawnCreep();
            this.t = 0;
            this.spawned++;
        } else if (this.t >= this._waveDuration) {
            this.t = 0;
            this.spawned = 0;
            this._waveSize++;
        }
    }
};