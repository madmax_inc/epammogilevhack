
const CELL_SIZE = 128;

export default class GameField {
    constructor(width, height) {
        this.width = width;
        this.height = height;

        this.towers = [];
        this.creeps = [];
        this.rounds = [];
    }


    spawnTower(tower, cell) {
        tower.setPos({ x: CELL_SIZE * (0.5 + cell.x), y: CELL_SIZE * (cell.y + 0.5) });
        this.towers.push(tower);
    }

    spawnCreep(creep) {
        const cell = {
            x: 0,
            y: 1
        };
        creep.setPos({ x: CELL_SIZE * (0.5 + cell.x), y: CELL_SIZE * (cell.y + 0.5) });
        this.creeps.push(creep);
    }
}