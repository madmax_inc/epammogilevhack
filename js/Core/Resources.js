export default {
    "dl": "resources/images/tiles/down-left_5x5_160x160.png",
    "empty": "resources/images/tiles/empty_4x4_128x128.png",
    "ul": "resources/images/tiles/left-up_5x5_160x160.png",
    "d": "resources/images/tiles/line-down_4x5_128x160.png",
    "l": "resources/images/tiles/line-left_5x4_160x128.png",
    "r": "resources/images/tiles/line-right_5x4_160x128.png",
    "u": "resources/images/tiles/line-up_4x5_128x160.png",
    "dr": "resources/images/tiles/right-down_5x5_160x160.png",
    "ur": "resources/images/tiles/right-up_5x5_160x160.png",


    "logo" : "resources/images/logo.svg",

    "tower1": "resources/images/towers/tower1.png",

    "creep1": "resources/images/creeps/creep1.png",

    "tower2_0": "resources/images/towers/gunAnim0.png",
    "tower2_1": "resources/images/towers/gunAnim1.png",
    "tower2_2": "resources/images/towers/gunAnim2.png",
    "tower2_3": "resources/images/towers/gunAnim3.png",
    "tower2_4": "resources/images/towers/gunAnim4.png",
    "tower2_5": "resources/images/towers/gunAnim5.png",
    "tower2_6": "resources/images/towers/gunAnim6.png",
    "tower2_7": "resources/images/towers/gunAnim7.png",
    "tower2_8": "resources/images/towers/gunAnim8.png",
    "tower2_9": "resources/images/towers/gunAnim9.png",
    "tower2_10": "resources/images/towers/gunAnim10.png",
    "tower2_11": "resources/images/towers/gunAnim11.png",
    "tower2_12": "resources/images/towers/gunAnim12.png",
    "tower2_13": "resources/images/towers/gunAnim13.png",
    "tower2_14": "resources/images/towers/gunAnim14.png",
    "tower2_15": "resources/images/towers/gunAnim15.png"
};