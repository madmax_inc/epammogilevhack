import * as PIXI from 'pixi.js'


export default class Viewport extends PIXI.Container {
    constructor() {
        super();

        this.mouseWheelHandler = this.mouseWheelHandler.bind(this);
        document.addEventListener("wheel", this.mouseWheelHandler, false);
    }

    mouseWheelHandler(e) {
        e = e || window.event;

        // wheelDelta не дает возможность узнать количество пикселей
        var delta = e.deltaY || e.detail || e.wheelDelta;

        e.preventDefault ? e.preventDefault() : (e.returnValue = false);

        const zoomFactor = delta / 1000;
        console.log(zoomFactor);

        console.log(this.scale);
        this.scale.x += zoomFactor;
        this.scale.y += zoomFactor;
    }
}