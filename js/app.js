import Game from './Core/Game'
import SplashScreenScene from "./Scenes/SplashScreenScene"

var game = new Game(() => {
    game.setScene(new SplashScreenScene(game));
});

document.body.appendChild(game.getView());