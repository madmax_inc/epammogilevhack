import * as PIXI from 'pixi.js'
import Path from "../Core/Path";
import Entity from "./Entity";

export default class Creep extends Entity {
    constructor(texture, props) {
        super(new PIXI.Sprite(texture));
        this._props = props;
        this._path = new Path();
        this._sprite = this._gameObj;
    }

    hit(amount) {
        this.props.hp -= amount;
    }


    isDead() {
        return this._props.hp <= 0.0;
    }

    setPos(pos) {
        this._sprite.x = pos.x;
        this._sprite.y = pos.y;
    }

    getPos() {
        return {
            x: this._sprite.x,
            y: this._sprite.y
        };
    }

    update(dt) {
        if (this._path) {
            const obj = this._path.advance(dt);
            const pos = obj.pos;
            const heading = obj.heading;
            this._sprite.rotation = heading;
            this._sprite.x = pos.x;
            this._sprite.y = pos.y;
        }
    }
}