import Creep from "../Creep";

const PLANE_PROPS = {
    hp: 100
};

export default class PlaneCreep extends Creep {
    constructor(texture) {
        super(texture, PLANE_PROPS);
    }
}