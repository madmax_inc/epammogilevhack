import * as PIXI from 'pixi.js'

export default class Entity {
    constructor(gameObj) {
        this._gameObj = gameObj;
        this._scene = null;
    }

    attach(scene) {
        this._scene = scene;
    }

    setPos(posObj) {
        this._gameObj.x = posObj.x;
        this._gameObj.y = posObj.y;
    }

    getPos() {
        return {
            x: this._gameObj.x,
            y: this._gameObj.y
        };
    }

    distanceFrom(posObj) {
        const myPos = this.getPos();


        return Math.hypot(myPos.x - posObj.x, myPos.y - posObj.y);
    }
}