import Tower from "../Tower";

const CANON_PROPS = {
    radius: 3.5,
    coolDown: 150
};

export default class Canon extends Tower {
    constructor(frames) {
        super(new PIXI.extras.AnimatedSprite(frames), CANON_PROPS);

        this._gameObj.anchor.x = 0.5;
        this._gameObj.anchor.y = 0.5;

        this._gameObj.animationSpeed = 0.5;
        this._gameObj.loop = false;
    }

    make_shot(creep) {
        this._gameObj.gotoAndPlay(0);
        super.make_shot(creep);
    }
}