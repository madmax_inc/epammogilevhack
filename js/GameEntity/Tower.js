import Entity from './Entity'

export default class Tower extends Entity {
    constructor(gameObj, props) {
        super(gameObj);
        this._props = props;
        this._cooldown = 0;
    }

    creepSpoted(creep) {
        var angel = Math.atan2(creep.getPos().y - this._gameObj.y, creep.getPos().x - this._gameObj.x) + Math.PI / 2;
        this._gameObj.rotation = angel;
        if(this._cooldown <= 0) {
            this.make_shot(creep);
        }
    }

    getRadius() {
        return this._props.radius;
    }

    update(dt) {
        if(this._cooldown > 0)
            --this._cooldown
    }

    make_shot(creep) {
        this._scene.spawnRound(creep);
        this._cooldown = this._props.coolDown;
    }
}