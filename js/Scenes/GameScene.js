import Scene from "../Core/Scene";
import * as Field from "../Core/Field"
import Viewport from '../Core/Viewport'
import CreepManager from "../Core/CreepManager";
import PlaneCreep from "../GameEntity/Creep/PlaneCreep";
import Canon from "../GameEntity/Tower/Canon";

const CELL_SIZE = 128;
const LARGE_CELL_SIZE = 160;

class CellDef {
    constructor(slug, xSlug, ySlug, resources) {
        const corner = (1.0 / 10.0);

        var anchorX = 0.5;
        var anchorY = 0.5;

        if (xSlug === "l") {
            anchorX += corner;
        } else if (xSlug === "r") {
            anchorX -= corner;
        }


        if (ySlug === "u") {
            anchorY += corner;
        } else if (ySlug === "d") {
            anchorY -= corner;
        }

        this.slug = slug;
        this.anchor = {
            "x" : anchorX,
            "y" : anchorY
        };

        const isLargeX = (xSlug !== "");
        const isLargeY = (ySlug !== "");

        this.size = {
            "x": isLargeX ? LARGE_CELL_SIZE : CELL_SIZE,
            "y": isLargeY ? LARGE_CELL_SIZE : CELL_SIZE
        };
        this.texture = resources[slug].texture;
    }
}

export default class GameScene extends Scene {
    /**
     * @param field GameField
     */
    constructor(field, creepManager, game) {
        super(game);

        this._field = field;
        //this._viewport = new Viewport();
        //this._viewport.x = 200;
        //this._viewport.y = 200;


        this._creepManager = creepManager;
        this._creepManager.setScene(this);
    }

    _getCellDef(cell, resources) {
        var yPart = "";
        var xPart = "";

        if (cell.y === 0) {
            yPart = "u";
        } else if (cell.y === (this._field.height - 1)) {
            yPart = "d";
        }

        if (cell.x === 0) {
            xPart = "l";
        } else if (cell.x === (this._field.width - 1)) {
            xPart = "r";
        }


        var resSlug = "empty";

        if (!(xPart === "" && yPart === "")) {
            resSlug = yPart + xPart;
        }

        return new CellDef(resSlug, xPart, yPart, resources);
    }

    init(resources) {
        this._planeTexture = resources.creep1.texture;

        this._canonFrames = [];
        for (var i = 0; i < 16; i++) {
            this._canonFrames.push(resources["tower2_" + i].texture);
        }

        this.backContainer = new PIXI.Container();

        for (var row = 0; row < this._field.height; row++) {
            for (var column = 0; column < this._field.width; column++) {
                const cellDef = this._getCellDef(new Field.FieldCell(column, row), resources);

                const sprite = new PIXI.Sprite(cellDef.texture);
                sprite.width = cellDef.size.x;
                sprite.height = cellDef.size.y;
                sprite.x = CELL_SIZE * (0.5 + column);
                sprite.y = CELL_SIZE * (0.5 + row);

                sprite.anchor.x = cellDef.anchor.x;
                sprite.anchor.y = cellDef.anchor.y;

                this.backContainer.addChild(sprite)
            }
        }

        this.backContainer.x = (LARGE_CELL_SIZE - CELL_SIZE);
        this.backContainer.y = (LARGE_CELL_SIZE - CELL_SIZE);
        this.addChild(this.backContainer);

        //this.addChild(this._viewport);

        //Init tickers
        this._game.getTicker().add((dt) => {
            this._updateCreeps(dt);
        });

        this._game.getTicker().add((dt) => {
            this._updateTowers(dt);
        });

        this._game.getTicker().add((dt) => {
            this._creepManager.update(dt);
        });

        this.spawnDemoTowers();
    }

    spawnCreep() {
        let creep = new PlaneCreep(this._planeTexture);
        this._field.spawnCreep(creep);
        this.backContainer.addChild(creep._gameObj);
    }

    spawnTower(tower, cell) {
        tower.attach(this);
        this._field.spawnTower(tower, cell);
        this.backContainer.addChild(tower._gameObj);
    }

    spawnDemoTowers() {
        this.spawnTower(new Canon(this._canonFrames), new Field.FieldCell(0, 0));
        this.spawnTower(new Canon(this._canonFrames), new Field.FieldCell(1, 0));
    }

    spawnRound(creep) {
        console.log('Fire');
        //TODO Malfunctional =(
        //this._field.spawnRound(creep);
    }

    _updateCreeps(dt) {

        for (const creepI in this._field.creeps) {
            this._field.creeps[creepI].update(dt);
        }

        for (const towerI in this._field.towers) {
            let tower = this._field.towers[towerI];
            const pos = tower.getPos();
            const radius = tower.getRadius();

            for (const creepI in this._field.creeps) {
                let creep = this._field.creeps[creepI];

                if (creep.distanceFrom(pos) < (radius * CELL_SIZE)) {
                    tower.creepSpoted(creep);
                    break;
                }
            }
        }
    }

    _updateTowers(dt) {
        for (const towerI in this._field.towers) {
            this._field.towers[towerI].update(dt);
        }
    }



}