import Scene from "../Core/Scene"
import * as PIXI from 'pixi.js'
import GameScene from "./GameScene";
import GameField from "../Core/GameField";
import CreepManager from "../Core/CreepManager";


export default class SplashScreenScene extends Scene {
    constructor(game) {
        super(game);
    }

    init(resources) {
        const logo = new PIXI.Sprite(resources.logo.texture);
        logo.anchor.set(0.5);


        console.log('Loading scene');


        logo.x = this._game.getScreen().width/2;
        logo.y = this._game.getScreen().height/2;

        this.addChild(logo);

        var transperenty = 0;
        var timer_ms = 0;
        var change = false;

        let f = (delta) => {
            timer_ms += delta;
            if(timer_ms < 100) {
                transperenty = timer_ms;
            } else if(timer_ms > 100 && timer_ms < 200) {
                transperenty = 100;
            } else {
                transperenty = 100 - (timer_ms - 200) ;
            }
            logo.alpha = transperenty/100;
            if(timer_ms != 0 && transperenty <= 0) {
                if(!change)
                    console.log('Change scene!');
                change = true;
                console.log(this);
                this._game.getTicker().remove(f);
                this._game.setScene(new GameScene(new GameField(8, 4), new CreepManager(3, 200, 1000), this._game));
            }
        };
        this._game.getTicker().add(f);


    }
}